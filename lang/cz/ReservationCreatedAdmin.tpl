{*
Copyright 2011-2019 Nick Korbel

This file is part of Booked Scheduler.

Booked Scheduler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Booked Scheduler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
*}
Podrobnosti rezervace:
<br/>
<br/>

Uživatel: {$UserName}<br/>: {$UserName}<br/>
{if !empty($CreatedBy)}
	Vytvořil: {$CreatedBy}
	<br/>
{/if}
Začátek: {formatdate date=$StartDate key=reservation_email}<br/>
Konec: {formatdate date=$EndDate key=reservation_email}<br/>
{if $ResourceNames|count > 1}
	Zdroje:
	<br/>
	{foreach from=$ResourceNames item=resourceName}
		{$resourceName}
		<br/>
	{/foreach}
{else}
	Zdroj: {$ResourceName}
	<br/>
{/if}

{if $ResourceImage}
	<div class="resource-image"><img src="{$ScriptUrl}/{$ResourceImage}"/></div>
{/if}

Název: {$Title}<br/>
Popis: {$Description|nl2br}

{if count($RepeatRanges) gt 0}
    <br/>
    Rezervováno pro tyto termíny:
    <br/>
{/if}

{foreach from=$RepeatRanges item=date name=dates}
    {formatdate date=$date->GetBegin()}
    {if !$date->IsSameDate()} - {formatdate date=$date->GetEnd()}{/if}
    <br/>
{/foreach}

{if $Participants|count >0}
    <br/>
    Účastníci:
    {foreach from=$Participants item=user}
        {$user->FullName()}
        <br/>
    {/foreach}
{/if}

{if $ParticipatingGuests|count >0}
    {foreach from=$ParticipatingGuests item=email}
        {$email}
        <br/>
    {/foreach}
{/if}

{if $Invitees|count >0}
    <br/>
    Pozváni:
    {foreach from=$Invitees item=user}
        {$user->FullName()}
        <br/>
    {/foreach}
{/if}

{if $InvitedGuests|count >0}
    {foreach from=$InvitedGuests item=email}
        {$email}
        <br/>
    {/foreach}
{/if}

{if $Accessories|count > 0}
	<br/>
	Příslušenství:
	<br/>
	{foreach from=$Accessories item=accessory}
		({$accessory->QuantityReserved}) {$accessory->Name}
		<br/>
	{/foreach}
{/if}

{if $Attributes|count > 0}
	<br/>
	{foreach from=$Attributes item=attribute}
		<div>{control type="AttributeControl" attribute=$attribute readonly=true}</div>
	{/foreach}
{/if}

{if $RequiresApproval}
	<br/>
	Nejméně jedna z rezervací vyžaduje schválení. Ujistěte se, že je schválena nebo odmítnuta.
{/if}

{if $CheckInEnabled}
	<br/>
	Nejméně jedna z rezervací vyžaduje potvrzení nebo zrušení rezervace.
	{if $AutoReleaseMinutes != null}
		Rezervace bude zrušena pokud ji uživatel nepotvrdí do {$AutoReleaseMinutes} minut od plánovaného začátku.
	{/if}
{/if}

<br/>
Referenční číslo: {$ReferenceNumber}

<br/>
<br/>
<a href="{$ScriptUrl}/{$ReservationUrl}">Zobrazit rezervaci</a> | <a href="{$ScriptUrl}">Přihlásit se do {$AppTitle}</a>
