{*
Copyright 2011-2019 Nick Korbel

This file is part of Booked Scheduler.

Booked Scheduler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Booked Scheduler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
*}
{include file='globalheader.tpl' cssFiles="https://cdn.rawgit.com/afeld/bootstrap-toc/v0.4.1/dist/bootstrap-toc.min.css"}


<div id="page-help">

    <div class="row">
        <div id="toc-div" class="col-sm-3 hidden-xs scrollspy">
            <nav id="toc" role="navigation" data-spy="affix" style="overflow-y: scroll;max-height:80%">
            </nav>
        </div>

        <div id="help" class="col-xs-12 col-sm-9">
            <h1>{$AppTitle} - nápověda</h1>

            <h2>Registrace</h2>

            <p>
                Chcete-li aplikaci používat, je potřeba se registrovat. Po
                vytvoření
                účtu
                se můžete přihlásit, čímž získáte přístup ke všem zdrojům, k nimž máte oprávnění.
            </p>

            <h2>Rezervace</h2>

            <p>
                V menu pod "Rozvrh" naleznete položku "Rezervovat". Zobrazí dostupné, rezervované a
                blokované termíny a umožní vám rezervaci
                zdrojů, ke kterým máte oprávnění.
            </p>

            <p>
                Na stránce "Rezervace" nalezněte zdroj, datum a čas zamýšlené rezervace. Kliknutím na časový úsek
                nastavíte podrobnosti rezervace. Tlačíto
                "Vytvořit" zkontroluje dostupnost, zarezervuje a odešle příslušné e-maily. Přidělené referenční číslo
                slouží ke sledování rezervace.
            </p>

            <p>Úpravy rezervace se uplatní až po použití tlačítka "Uložit".</p>

            <p>Ve výchozím nastavení je pouze správce aplikace oprávněn rezervovat do minulosti.</p>

            <h3>Najít termín</h3>
            <p>
                V menu "Rozvrh" je volba "Najít termín". Umožňuje najít volný časový úsek
                dle vašich požadavků.
            </p>

            <h3>Více zdrojů</h3>

            <p>Lze rezervovat všechny zdroje coby součást jediné rezervace, samozřejmě máte-li k nim oprávnění. Pro přidání více zdrojů do
                rezervace stačí kliknout na odkaz "nastavit" vedle popisku "Zdroje".
                Více zdrojů vyberete jejich označením a potvrzením tlačítkem "Hotovo".</p>

            <p>Odebrání provedete stejným postupem, jen příslušný zdroj odznačníte
                a ukončíte tlačítkem "Hotovo".</p>

            <p>Přidávané zdroje musí mít rezervační pravidla shodná s primárním. To znamená,
                že pokusíte-li se o rezervaci 2 hodiny u "Zdroj 1", který umožňuje maximálně tří hodinovou rezervaci
                současně se "Zdroj 2"
                s maximálně hodinovou rezervací, rezervace bude zamítnuta.</p>

            <p>Detailní vlastnosti zdrojů se zobrazí po najetí na jejich název.</p>

            <h3>Opakování</h3>

            <p>Opakování lze zadat mnoha způsoby. Položka "konec opakované rezervace" je součástí
                všech.</p>

            <p>Opakování zjednodušuje zadávání pravidelných rezervací. Například:
                <ul><li>"denní" každý 2 den - bude rezervovat obden ve zvolený čas</li> Repeat Weekly, every 1 week on Monday, Wednesday,
                <li>"týdenní" každý 1. týden v Po, St - zarezervuje každý týden v příslušné dny a čas</li>
                <li>"měsíční" každý 3. měsíc a volbou "den měsíce" se začátkem 2011-01-15 - vytvoří rezervace pro každý třetí měsíc a jeho patnáctý den</li>
                <li>"měsíční" každý 3. měsíc a volbou "den týdne" se začátkem 2011-01-15 (což je třetí sobota) - vytvoří rezervace pro každý třetí měsíc a jeho třetí sobotu</li>
                </ul></p>

            <h3>Additional Participants</h3>

            <p>V průběhu rezervace můžete přidat účastníky nebo pozvané. Osoby přidané mezi účastníky se jimi stávají, 
                aniž by museli reagovat na zaslanou pozvánku, 
                kterou obdrží e-mailem. Pozvaní si mohou vybrat, zda pozvánku
                přijmou nebo odmítnou. Přijetím
                je uživatel přidán k účastníkům. Odmítnutím je odebrán z 
                pozvaných.
            </p>

            <p>
                Počet účastníků je omezen kapacitou zdroje.
            </p>

            <h3>Vybavení</h3>

            <p>Na vybavení lze pohlížet jako na předměty užívané během rezervace. Například projektory nebo židle. Chcete-li
                své rezervaci vybavení přidat, klikněte na odkaz "přidat" napravo od popisku "Vybavení". Následně 
                si budete moct zvolit množství jednotivých položek vybavení. Dostupný počet 
                zavisí tom, kolik vybavení již bylo rezervováno.</p>

            <h3>Rezervace v zastoupení</h3>

            <p>Správci aplikace a skupina Správci mohou rezervovat jménem jiných uživatelů kliknutím na 
                odkaz "nastavit" vpravo od jména uživatele.</p>

            <p>Správci aplikace a skupina Správci mohou také odstranit rezervace jiných
                uživatelů.</p>

            <h2>Aktualizace rezervace</h2>

            <p>Můžete aktualizovat libovolnou vámi vytvořenou rezervaci nebo tu, která byla vytvořena pro vás.</p>

            <h3 data-toc-text="Jednotlivé výskyty">Aktualizace jednotlivých výskytů opakování</h3>

            <p>
                Má-li rezervace nastaveno opakování, je vytvořena jako série. Uložíte-li provedené změny, 
                budete dotázáni, které výskyty se mají změnit. Můžete zvolit
                <ul><li>"tento výskyt" - upraví aktuálně zobrazený výskyt a všechny ostatní ponechá bezezměny</li>
                <li>"všechny výskyty" - aktualizuje všechny výskyty, které dosud neproběhly</li>
                <li>"tento a budoucí" - změní aktkuálně zobrazený a všechny následující výskyty</li>
                </ul>
            </p>

            <p>Pouze správci mohou upravovat již proběhlé rezervace.</p>

            <h2>Odstranění rezervace</h2>

            <p>Odstranění rezervace vede k úplnému vyjmutí z rozvrhu. V aplikaci 
                {$AppTitle} již položka nebude dále figurovat.</p>

            <h3 data-toc-text="Specific Instances">Odstanění konkrétních výskytů opakování</h3>

            <p>Obdobně jako při aktualizaci rezervaci budete mít při odstraňování možnost výběru konkrétních výskytů.</p>

            <p>Pouze správci mohou odstraňovat již proběhlé rezervace.</p>

            <h2>Kredity</h2>

            <p>Kredity dávají správcům kontrolu nad využíváním zdrojů. Zdroj může být nakonfigurován tak, aby vyžadoval určitý počet
                kreditů na časový úsek. Při nedostatku kreditů rezervaci nepůjde provést. Stav vašich kreditů zjistíte v
                menu "Můj účet", části "Kredity"</p>

            <h2>Placené rezervace</h2>
            <p>Rezervace mohou být hrazeny prostřednictvím kreditů. Nemáte-li jich na rezervaci dostatek, můžete je dokoupit v menu "Můj účet", části "Kredity".
                 Historii nákupů a stav kreditů zobrazíte tamtéž.

            <h2 data-toc-text="Add to Calendar">Přidání rezervace do kalendáře (Outlook&reg;, iCal, Mozilla Lightning,
                Evolution)</h2>

            <p>Při prohlížení nebo aktualizaci rezervace se zobrazí tlačítko "Přidat do aplikace Outlook". Máte-li aplikaci Outlook nainstalovánu na svém počítači,
		měl by vás vyzvat k přidání schůzky. Není-li nainstalována, budete vyzváni ke stažení
                .ics soubor, což je standardní formát pro kalendářové položky. Soubor pak použijete pro vložení rezervace v libovolné aplikaci,
                která podporuje formát iCalendar.</p>

            <h2>Odběr kalendářů</h2>

            <p>Kalendáře lze publikovat pro rozvrhy, zdroje a uživatele. Funci aktivuje správce
                
                nastavením předplatitelského klíče v konfiguračním souboru. Povolení kalendářů na úrovni rozvrhů a zdrojů
                je otázkou prostého povolení veřejné viditelnosti při správě rozvrhu nebo zdroje. Pro odběr osobního kalendáře
                použijte Rozvrh > Můj kalendář. Na pravé straně stránky naleznete odkaz "zapnout" nebo "vypnout"
                odběr kalendáře.
            </p>

            <p> Chcete-li se přihlásit k odběru kalendáře, otevřete menu "Rozvrh" > "Kalendář zdrojů" a vyberte ten, který potřebujete.
                V pravé části stránky najdete odkaz pro odběr zobrazeného kalendáře. Stejným postupem nastavíte odběr
                kalendáře všech zdrojů. Pro odběr osobního kalendář, použijte menu "Rozvrh" > "Můj kalendář". Také zde
                najdete v pravé části stránky odkaz pro odběr.</p>

            <p> Ve výchozím nastavení budou zobrazeny události pro nadcházejících 30 dnů. To lze upravit pomocí
                dvou parametrů v URL odběru, kde "pastDayCount" a "futureDayCount" určují
                počet načtených minulých a budoucí dnů.</p>

            <h3 data-toc-text="Calendar Client">Kalednářoví klienti (Outlook&reg;, iCal, Mozilla Lightning, Evolution)</h3>

            <p> Ve většině případů prosté klepnutí na odkaz "Odběr kalendáře" automaticky nastaví jeho odběr vaším 
                kalendářovým klientem. Není-li aplikací Outlook přidán automaticky, otevřete kalendář a klepněte pravým tlačítkem myši
                na "Mé kalendáře" a vyberte
                Přidat kalendář > Internetový kalendář. Vložte URL adresu zobrazenou odkazem "Odběr kalendáře"
                . </p>

            <h3>Google&reg; kalendář</h3>

            <p>Otevřete nastavení Google kalednáře. Klikněte na kartu "Kalendáře". Klikněte na "Procházet kalendáře" a pak "Přidat jako URL".
                Vložte
                URL adresu zobrazenou odkazem "Odběr kalendáře"            .</p>

            <h3>Externí vkládání kalendářů</h3>
            <p class="note">Na serveru je potřeba povolit CORS. Následujíc přidejte do Apache htaccess souboru <code>Header Set Access-Control-Allow-Origin "*"</code></p>
            <p>Vložení Booked kalednáře na externí web je snadné. Zkopírujte a vložte následující
                JavaScript
                na svůj web
                <code>
                    &lt;script async src=&quot;{$ScriptUrl}/scripts/embed-calendar.js&quot; crossorigin=&quot;anonymous&quot;&gt;&lt;/script&gt;
                </code>
            </p>

            <p>Následujícími parametry dotazu můžete přizpůsobit vložený pohled:</p>

            <table class="table">
                <thead>
                <tr>
                    <th>Název</th>
                    <th>Přípustné hodnoty</th>
                    <th>Výchozí</th>
                    <th>Podrobnosti</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>type</td>
                    <td>agenda, week, month</td>
                    <td>agenda</td>
                    <td>Definuje zobrazený pohled</td>
                </tr>
                <tr>
                    <td>format</td>
                    <td>date, title, user, resource</td>
                    <td>date</td>
                    <td>Definuje informace zobrazené v rezervačním okně. Lze předat více parametrů. Např. datum a nadpsi zadáním date,title</td>
                </tr>
                <tr>
                    <td>d</td>
                    <td>Celá čísla mezi 1 a 30</td>
                    <td>7</td>
                    <td>Omezuje počet zobrazovaných dnů</td>
                </tr>
                <tr>
                    <td>sid</td>
                    <td>Any schedule public ID</td>
                    <td>All schedules</td>
                    <td>Omezuje rezervace zobrazované daným rozvrhem</td>
                </tr>
                <tr>
                    <td>rid</td>
                    <td>Any resource public ID</td>
                    <td>All resources</td>
                    <td>Omezuje rezervace zobrazované pro daný zdroj</td>
                </tr>
                </tbody>
            </table>

            <p><strong>Budou zobrazeny pouze kalendáře a zdroje nastavené jako veřejné.</strong> Pokud
                rezervace
                v rozvrhu nebo zdroji chybí, pak nejspíš nebyly nastaveny jako veřejné.</p>

            <h2>Kvóty</h2>

            <p>Správci mají možnost konfigurovat pravidla kvót na základě různých kritérií. Dojde-li při rezervaci
                k porušení jakékoli kvóty, budete na to upozorněni a rezervace bude odepřena.</p>

            <h2>Čekání na dostupnost</h2>

            <p>Není-li požadovaný čas k dispozici, můžete být upozorněni na případnou změnu. Tato možnost se zobrazí
                po neúspěšném pokusu o rezervaci.</p>


        </div>
    </div>
</div>
<script src="https://cdn.rawgit.com/afeld/bootstrap-toc/v0.4.1/dist/bootstrap-toc.min.js"></script>
{include file="javascript-includes.tpl"}
<script type="text/javascript">
    $(function () {
        var navSelector = '#toc';
        var $myNav = $(navSelector);
        Toc.init({
            $nav: $myNav,
            $scope: $('#help')
        });

        $('body').scrollspy({
            target: navSelector,
            offset: 50
        });
    });
</script>
{include file='globalfooter.tpl'}
