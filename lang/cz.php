<?php
/**
 * Copyright 2011-2019 Nick Korbel
 *
 * This file is part of Booked Scheduler.
 *
 * Booked Scheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Booked Scheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('en_us.php');

class cz extends en_us
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @return array
	 */
	protected function _LoadDates()
	{
		$dates = array();

		$dates['general_date'] = 'Y/m/d';
		$dates['general_datetime'] = 'Y/m/d g:i:s A';
		$dates['short_datetime'] = 'y/n/j g:i A';
		$dates['schedule_daily'] = 'l, y/n/j';
		$dates['reservation_email'] = 'Y/m/d @ g:i A (e)';
		$dates['res_popup'] = 'D, n/d g:i A';
		$dates['res_popup_time'] = 'g:i A';
		$dates['short_reservation_date'] = 'y/n/j g:i A';
		$dates['dashboard'] = 'D, n/d g:i A';
		$dates['period_time'] = 'g:i A';
		$dates['timepicker'] = 'h:i a';
		$dates['mobile_reservation_date'] = 'n/j g:i A';
		$dates['general_date_js'] = 'yy/mm/dd';
        $dates['general_time_js'] = 'h:mm tt';
        $dates['timepicker_js'] = 'h:i a';
        $dates['momentjs_datetime'] = 'YY/M/D/YY h:mm A';
		$dates['calendar_time'] = 'h:mmt';
		$dates['calendar_dates'] = 'M d';
		$dates['embedded_date'] = 'D d';
		$dates['embedded_time'] = 'g:i A';
		$dates['embedded_datetime'] = 'n/j g:i A';

		$this->Dates = $dates;

		return $this->Dates;
	}

	/**
	 * @return array
	 */
	protected function _LoadStrings()
	{
		$strings = array();

		$strings['FirstName'] = 'Jméno';
		$strings['LastName'] = 'Příjmení';
		$strings['Timezone'] = 'Časová zóna';
		$strings['Edit'] = 'Upravit';
		$strings['Change'] = 'nastavit';
		$strings['Rename'] = 'Přejmenovat';
		$strings['Remove'] = 'Odstranit';
		$strings['Delete'] = 'Smazat';
		$strings['Update'] = 'Uložit';
		$strings['Cancel'] = 'Zrušit';
		$strings['Add'] = 'přidat';
		$strings['Name'] = 'Název';
		$strings['Yes'] = 'Ano';
		$strings['No'] = 'Ne';
		$strings['FirstNameRequired'] = 'Jméno je povinné.';
		$strings['LastNameRequired'] = 'Příjmení je povinné.';
		$strings['PwMustMatch'] = 'Hesla se neshodují.';
		$strings['ValidEmailRequired'] = 'E-mailová adresa je povinná.';
		$strings['UniqueEmailRequired'] = 'Tato e-mailová adresa je již v systému zaregistrována.';
		$strings['UniqueUsernameRequired'] = 'Toto uživatelské jméno je již v systému zaregistrované.';
		$strings['UserNameRequired'] = 'Uživatelské jméno je povinné.';
		$strings['CaptchaMustMatch'] = 'Opište bezpečnostní kód z obrázku.';
		$strings['Today'] = 'Dnes';
		$strings['Week'] = 'Týden';
		$strings['Month'] = 'Měsíc';
		$strings['BackToCalendar'] = 'Zpět do rozvrhu';
		$strings['BeginDate'] = 'Začátek';
		$strings['EndDate'] = 'Konec';
		$strings['Username'] = 'Uživatelské jméno';
		$strings['Password'] = 'Heslo';
		$strings['PasswordConfirmation'] = 'Potvrdit heslo';
		$strings['DefaultPage'] = 'Výchozí hlavní stránka';
		$strings['MyCalendar'] = 'Můj rozvrh';
		$strings['ScheduleCalendar'] = 'Rozvrh';
		$strings['Registration'] = 'Registrace';
		$strings['NoAnnouncements'] = 'Není naplánována žádná odstávka.';
		$strings['Announcements'] = 'Odstávky';
		$strings['NoUpcomingReservations'] = 'Nemáte naplánované žádné rezervace.';
		$strings['UpcomingReservations'] = 'Vaše naplánované rezervace';
		$strings['AllNoUpcomingReservations'] = 'Žádné naplánované rezervace.';
		$strings['AllUpcomingReservations'] = 'Všechny naplánované rezervace.';
		$strings['ShowHide'] = 'Zobrazit/skrýt';
		$strings['Error'] = 'Chyba';
		$strings['ReturnToPreviousPage'] = 'Vrátit se zpět';
		$strings['UnknownError'] = 'Neznámá chyba';
		$strings['InsufficientPermissionsError'] = 'Nemáte oprávnení';
		$strings['MissingReservationResourceError'] = 'Nebyl vybrán zdroj';
		$strings['MissingReservationScheduleError'] = 'Nebyl zaškrtnut žádný den';
		$strings['DoesNotRepeat'] = 'neopakovat';
		$strings['Daily'] = 'denní';
		$strings['Weekly'] = 'týdenní';
		$strings['Monthly'] = 'měsíční';
		$strings['Yearly'] = 'roční';
		$strings['RepeatPrompt'] = 'Opakování';
		$strings['hours'] = 'hodina';
		$strings['days'] = '. den';
		$strings['weeks'] = '. týden';
		$strings['months'] = '. měsíc';
		$strings['years'] = '. rok';
		$strings['day'] = 'den';
		$strings['week'] = 'týden';
		$strings['month'] = 'měsíc';
		$strings['year'] = 'rok';
		$strings['repeatDayOfMonth'] = 'den měsíce';
		$strings['repeatDayOfWeek'] = 'den týdne';
		$strings['RepeatUntilPrompt'] = 'konec opakované rezervace';
		$strings['RepeatEveryPrompt'] = 'každý';
		$strings['RepeatDaysPrompt'] = 'Opakovat v dny';
		$strings['CreateReservationHeading'] = 'Vytváření rezervace';
		$strings['EditReservationHeading'] = 'Úprava rezervace: %s';
		$strings['ViewReservationHeading'] = 'Zobrazit rezervaci: %s';
		$strings['ReservationErrors'] = 'Nastavit rezervaci';
		$strings['Create'] = 'Vytvořit';
		$strings['ThisInstance'] = 'tento výskyt';
		$strings['AllInstances'] = 'všechny výskyty';
		$strings['FutureInstances'] = 'budoucí výskyty';
		$strings['Print'] = 'Tisknout';
		$strings['ShowHideNavigation'] = 'zobrazit/skrýt navigaci';
		$strings['ReferenceNumber'] = 'Referenční číslo';
		$strings['Tomorrow'] = 'Zítra';
		$strings['LaterThisWeek'] = 'Později v tento týden';
		$strings['NextWeek'] = 'Následující týden';
		$strings['SignOut'] = 'Odhlášení';
		$strings['LayoutDescription'] = 'Začátek %s, zobrazeno %s dnů';
		$strings['AllResources'] = 'Všechny zdroje';
		$strings['TakeOffline'] = 'Vypnout';
		$strings['BringOnline'] = 'Zapnout';
		$strings['AddImage'] = 'Přidat obrázek';
		$strings['NoImage'] = 'Žádný obrázek';
		$strings['Move'] = 'Přesunout';
		$strings['AppearsOn'] = 'Objeví se na %s';
		$strings['Location'] = 'Umístění';
		$strings['NoLocationLabel'] = '(umístění nenastaveno)';
		$strings['Contact'] = 'Kontakt';
		$strings['NoContactLabel'] = '(kontakt nenastaven)';
		$strings['Description'] = 'Popis';
		$strings['NoDescriptionLabel'] = '(bez popisku)';
		$strings['Notes'] = 'Poznámky';
		$strings['NoNotesLabel'] = '(bez poznámek)';
		$strings['NoTitleLabel'] = '(bez názvu)';
		$strings['UsageConfiguration'] = 'Použít konfiguraci';
		$strings['ChangeConfiguration'] = 'Nastavit konfiguraci';
		$strings['ResourceMinLength'] = 'Rezervace musí být delší než %s';
		$strings['ResourceMinLengthNone'] = 'Neexistuje žádná minimální doba rezervace';
		$strings['ResourceMaxLength'] = 'Rezervace musí být kratší než %s';
		$strings['ResourceMaxLengthNone'] = 'Neexistuje žádná maximální doba rezervace';
		$strings['ResourceRequiresApproval'] = 'Rezervace musí být schváleny';
		$strings['ResourceRequiresApprovalNone'] = 'Rezervace není třeba potvrzovat';
		$strings['ResourcePermissionAutoGranted'] = 'Povolení je automaticky získáno';
		$strings['ResourcePermissionNotAutoGranted'] = 'Povolení není automaticky získáno';
		$strings['ResourceMinNotice'] = 'Rezervace musí být uskutečněna nejméně %s před začátkem';
		$strings['ResourceMinNoticeNone'] = 'Rezervace lze provést až do současné doby';
		$strings['ResourceMinNoticeUpdate'] = 'Rezervaci lze změnit nejpozději %s před začátkem';
		$strings['ResourceMinNoticeNoneUpdate'] = 'Rezervace lze provést až do současné doby';
		$strings['ResourceMinNoticeDelete'] = 'Rezervaci lze zrušit nejpozději %s před začátkem';
		$strings['ResourceMinNoticeNoneDelete'] = 'Rezervace lze provést až do současné doby';
		$strings['ResourceMaxNotice'] = 'Rezervace nesmí končit více než %s před současností';
		$strings['ResourceMaxNoticeNone'] = 'Rezervace může skončit kdykoliv v budoucnu';
		$strings['ResourceBufferTime'] = 'Mezi rezervacemi musí být pauza %s.';
		$strings['ResourceBufferTimeNone'] = 'Mezi rezervacemi není pauza.';
		$strings['ResourceAllowMultiDay'] = 'Rezervace může být vytvořena na několik dnů';
		$strings['ResourceNotAllowMultiDay'] = 'Rezervace nelze provádět v rámci několika dnů';
		$strings['ResourceCapacity'] = 'Tento zdroj má omezenou kapacitu na %s osob';
		$strings['ResourceCapacityNone'] = 'Tento zdroj má neomezenou kapacitu';
		$strings['AddNewResource'] = 'Přidat nový zdroj';
		$strings['AddNewUser'] = 'Přidat nového uživatele';
		$strings['AddResource'] = 'Přidat zdroj';
		$strings['Capacity'] = 'Kapacita';
		$strings['Access'] = 'Přístup';
		$strings['Duration'] = 'Trvání';
		$strings['Active'] = 'Aktivní';
		$strings['Inactive'] = 'Vypnuto';
		$strings['ResetPassword'] = 'Obnovit heslo';
		$strings['LastLogin'] = 'Poslední přihlášení';
		$strings['Search'] = 'Hledání';
		$strings['ResourcePermissions'] = 'Oprávnění zdrojů';
		$strings['Reservations'] = 'Rezervace';
		$strings['Groups'] = 'Skupiny';
		$strings['Users'] = 'Uživatelé';
		$strings['AllUsers'] = 'Všichni uživatelé';
		$strings['AllGroups'] = 'Všechny skupiny';
		$strings['AllSchedules'] = 'Všechny rozvrhy';
		$strings['UsernameOrEmail'] = 'Uživatelské jméno nebo e-mail';
		$strings['Members'] = 'Členové';
		$strings['QuickSlotCreation'] = 'Vytvořit místo každých %s minut mezi %s a %s';
		$strings['ApplyUpdatesTo'] = 'aktualizovat';
		$strings['CancelParticipation'] = 'Zrušení účastníků';
		$strings['Attending'] = 'Účast';
		$strings['QuotaConfiguration'] = 'pro %s pro %s uživatelům v %s omezení počtu %s %s na %s';
		$strings['QuotaEnforcement'] = 'Vynutit %s %s';
		$strings['reservations'] = 'rezervací';
		$strings['reservation'] = 'rezervace';
		$strings['ChangeCalendar'] = 'Nastavit rozvrh';
		$strings['AddQuota'] = 'Přdat kvótu';
		$strings['FindUser'] = 'Najít uživatele';
		$strings['Created'] = 'Vytvořeno';
		$strings['LastModified'] = 'Poslední úprava';
		$strings['GroupName'] = 'Název skupiny';
		$strings['GroupMembers'] = 'Členové skupiny';
		$strings['GroupRoles'] = 'Role skupiny';
		$strings['GroupAdmin'] = 'Administrátor skupiny';
		$strings['Actions'] = 'Akce';
		$strings['CurrentPassword'] = 'Současné heslo';
		$strings['NewPassword'] = 'Nové heslo';
		$strings['InvalidPassword'] = 'Bylo chybně zadáno současné heslo.';
		$strings['PasswordChangedSuccessfully'] = 'Vaše nové heslo bylo úspěšně nastaveno';
		$strings['SignedInAs'] = 'Přihlášen jako';
		$strings['NotSignedIn'] = 'Nepřihlášený';
		$strings['ReservationTitle'] = 'Název rezervace';
		$strings['ReservationDescription'] = 'Volitelný popis';
		$strings['ResourceList'] = 'Rezervované zdroje';
		$strings['Accessories'] = 'Vybavení';
		$strings['InvitationList'] = 'Pozvánky';
		$strings['AccessoryName'] = 'Název vybavení';
		$strings['QuantityAvailable'] = 'Množství';
		$strings['Resources'] = 'Zdroje';
		$strings['Participants'] = 'Účastníci';
		$strings['User'] = 'Uživatel';
		$strings['Resource'] = 'Zdroj';
		$strings['Status'] = 'Stav';
		$strings['Approve'] = 'Schválit';
		$strings['Page'] = 'Strana';
		$strings['Rows'] = 'Řádek';
		$strings['Unlimited'] = 'neomezeno';
		$strings['Email'] = 'E-mail';
		$strings['EmailAddress'] = 'E-mailová adresa';
		$strings['Phone'] = 'Telefon';
		$strings['Organization'] = 'Společnost';
		$strings['Position'] = 'Pozice';
		$strings['Language'] = 'Jazyk';
		$strings['Permissions'] = 'Oprávnění';
		$strings['Reset'] = 'Vyprázdnit';
		$strings['FindGroup'] = 'Najít skupinu';
		$strings['Manage'] = 'Nastavit';
		$strings['None'] = 'žádné';
		$strings['AddToOutlook'] = 'Přidat do aplikace Outlook';
		$strings['Done'] = 'Hotovo';
		$strings['RememberMe'] = 'Trvalé přihlášení';
		$strings['FirstTimeUser?'] = 'Nemáte založen účet?';
		$strings['CreateAnAccount'] = 'Registrovat se';
		$strings['ViewSchedule'] = 'Zobrazit rezervace';
		$strings['ForgotMyPassword'] = 'Zapomenuté heslo';
		$strings['YouWillBeEmailedANewPassword'] = 'Na zadaný e-mail Vám bude zasláno nově vygenerované heslo.';
		$strings['Close'] = 'Zavřít';
		$strings['ExportToCSV'] = 'Exportovat do souboru CSV';
		$strings['OK'] = 'Odeslat';
		$strings['Working'] = 'Načítám';
		$strings['Login'] = 'Přihlášení';
		$strings['AdditionalInformation'] = 'Další informace.';
		$strings['AllFieldsAreRequired'] = 'Všechna pole jsou povinná';
		$strings['Optional'] = 'Nepovinné';
		$strings['YourProfileWasUpdated'] = 'Váš profil by aktualizován.';
		$strings['YourSettingsWereUpdated'] = 'Vaše nastavení bylo provedeno';
		$strings['Register'] = 'Registrovat';
		$strings['SecurityCode'] = 'Bezpečnostní kód';
		$strings['ReservationCreatedPreference'] = 'Vytvořím-li rezervaci, nebo je-li pro mě vytvořena';
		$strings['ReservationUpdatedPreference'] = 'Změním-li rezervaci, nebo je-li má změněna';
		$strings['ReservationDeletedPreference'] = 'Smažu-li rezervaci, nebo je-li má smazána';
		$strings['ReservationApprovalPreference'] = 'Je-li má rezervace potvrzena';
		$strings['PreferenceSendEmail'] = 'Oznámit e-mailem';
		$strings['PreferenceNoEmail'] = 'Neoznamovat';
		$strings['ReservationCreated'] = 'Rezervace byla vytvořena.';
		$strings['ReservationUpdated'] = 'Rezervace byla upravena.';
		$strings['ReservationRemoved'] = 'Rezervace byla odstraněna.';
		$strings['ReservationRequiresApproval'] = 'Jedna nebo více rezervací vyžadují schválení. Do té doby budou označeny jako nevyřízené.';
		$strings['YourReferenceNumber'] = 'Referenční číslo: %s';
		$strings['ChangeUser'] = 'Nastavit uživatele';
		$strings['MoreResources'] = 'Přidat další zdroj';
		$strings['ReservationLength'] = 'Délka rezervace';
		$strings['ParticipantList'] = 'Seznam účastníků';
		$strings['AddParticipants'] = 'Přidat účastníka';
		$strings['InviteOthers'] = 'Pozvat ostatní';
		$strings['AddResources'] = 'Nastavit zdroje';
		$strings['AddAccessories'] = 'Přidat vybavení';
		$strings['Accessory'] = 'Vybavení';
		$strings['QuantityRequested'] = 'Požadované množství';
		$strings['CreatingReservation'] = 'Vytváření rezervace';
		$strings['UpdatingReservation'] = 'Úprava rezervace';
		$strings['DeleteWarning'] = 'Tato akce je trvalá a nelze ji vrátit zpět!';
		$strings['DeleteAccessoryWarning'] = 'Při odstranění tohoto vybavení jej odstraníte také ze všech rezervací.';
		$strings['AddAccessory'] = 'Přidat vybavení';
		$strings['AddBlackout'] = 'Přidat odstávku';
		$strings['AllResourcesOn'] = 'všechny zdroje na';
		$strings['Reason'] = 'Odůvodnění';
		$strings['BlackoutShowMe'] = 'Zobrazit odstávky, které jsou v konfliktu s jinými';
		$strings['BlackoutDeleteConflicts'] = 'Smazat odstávky, které jsou v konfliktu s jinými';
		$strings['Filter'] = 'Filtr';
		$strings['Between'] = 'Mezi';
		$strings['CreatedBy'] = 'Vytvořil';
		$strings['BlackoutCreated'] = 'Odstávka byla nastavena.';
		$strings['BlackoutNotCreated'] = 'Odstávka nebyla nastavena';
		$strings['BlackoutUpdated'] = 'Odstávka byla upravena.';
		$strings['BlackoutNotUpdated'] = 'Odstávka nebyla upravena.';
		$strings['BlackoutConflicts'] = 'Konflikty odstávek';
		$strings['ReservationConflicts'] = 'Konflikty v časech rezervací';
		$strings['UsersInGroup'] = 'Uživatelé v této skupině';
		$strings['Browse'] = 'Prohledat';
		$strings['DeleteGroupWarning'] = 'Odstraněním této skupiny budou odstraněny všechny související oprávnění k zdrojům. Uživatelé v této skupině mohou přijít o přístup ke zdrojům.';
		$strings['WhatRolesApplyToThisGroup'] = 'Jaká role se vztahuje k této skupině?';
		$strings['WhoCanManageThisGroup'] = 'Kdo může spravovat tuto skupinu?';
		$strings['WhoCanManageThisSchedule'] = 'Kdo může spravovat tento rozvrh?';
		$strings['AllQuotas'] = 'Všechny kvóty';
		$strings['QuotaReminder'] = 'Nezapomeňte: Kvóty se uplatňují na základě nastavení časového pásma.';
		$strings['AllReservations'] = 'Všechny rezervace';
		$strings['PendingReservations'] = 'Nevyřízené rezervace';
		$strings['Approving'] = 'Schvalování';
		$strings['MoveToSchedule'] = 'Přesunout do rozvrhu';
		$strings['DeleteResourceWarning'] = 'Odstraněním tohoto zdroje vymažete všechna související data.';
		$strings['DeleteResourceWarningReservations'] = 'všechny minulé, současné a budoucí rezervace s ním spojené';
		$strings['DeleteResourceWarningPermissions'] = 'všechna přiřazená povolení';
		$strings['DeleteResourceWarningReassign'] = 'Prosím přeřaďte cokoli, co nechcete, aby bylo vymazáno.';
		$strings['ScheduleLayout'] = 'Rozvržení (časy v %s)';
		$strings['ReservableTimeSlots'] = 'Rezervovatelné časové úseky';
		$strings['BlockedTimeSlots'] = 'Blokovaný časový úsek';
		$strings['ThisIsTheDefaultSchedule'] = 'Toto je výchozí rozvrh';
		$strings['DefaultScheduleCannotBeDeleted'] = 'výchozí rozvrh nemůže být smazán';
		$strings['MakeDefault'] = 'Vytvořit jako výchozí';
		$strings['BringDown'] = 'Snížit';
		$strings['ChangeLayout'] = 'Nastavit rozvržení';
		$strings['AddSchedule'] = 'Přidat rozvrh';
		$strings['StartsOn'] = 'Začíná od';
		$strings['NumberOfDaysVisible'] = 'Viditelné dnů';
		$strings['UseSameLayoutAs'] = 'Použít rozvržení jako';
		$strings['Format'] = 'Formát';
		$strings['OptionalLabel'] = 'Nepovinné pole';
		$strings['LayoutInstructions'] = 'Vložte každý časový úsek na jeden řádek. Časové úseky musí být naplánovány na celý den - 24hodin';
		$strings['AddUser'] = 'Přidat uživatele';
		$strings['UserPermissionInfo'] = 'Aktuální přístup ke zdroji se může lišit v závislosti na roli uživatele a skupiny oprávnění nebo externím nastavení oprávnění';
		$strings['DeleteUserWarning'] = 'Po odstranění tohoto uživatele odstraníte také jeho všechny současné, budoucí a minulé rezervace.';
		$strings['AddAnnouncement'] = 'Naplánovat odstávku';
		$strings['Announcement'] = 'Oznámení';
		$strings['Priority'] = 'Priorita';
		$strings['Reservable'] = 'Volné';
		$strings['Unreservable'] = 'Zavřeno';
		$strings['Reserved'] = 'Rezervováno';
		$strings['MyReservation'] = 'Mé rezervace';
		$strings['Pending'] = 'Před schválením';
		$strings['Past'] = 'Zmeškané';
		$strings['Restricted'] = 'Odstávka';
		$strings['ViewAll'] = 'Zobrazit vše';
		$strings['MoveResourcesAndReservations'] = 'Přesunout zdroje a rezervace do';
		$strings['TurnOffSubscription'] = 'Vypnout odběr rozvrhu';
		$strings['TurnOnSubscription'] = 'Zapnout odběr rozvrhu';
		$strings['SubscribeToCalendar'] = 'Odběr rozvrhu';
		$strings['SubscriptionsAreDisabled'] = 'Administrátor zakázal odběr rozvrhu';
		$strings['NoResourceAdministratorLabel'] = '(Žádné administrátorské zdroje)';
		$strings['WhoCanManageThisResource'] = 'Kdo může spravovat tyto zdroje?';
		$strings['ResourceAdministrator'] = 'Administrátorské zdroje';
		$strings['Private'] = 'Rezervováno';
		$strings['Accept'] = 'Přijmout';
		$strings['Decline'] = 'Odmítnout';
		$strings['ShowFullWeek'] = 'Zobrazit celý týden';
		$strings['CustomAttributes'] = 'Vlastní pole';
		$strings['AddAttribute'] = 'Přidat pole';
		$strings['EditAttribute'] = 'Upravit pole';
		$strings['DisplayLabel'] = 'Popisek';
		$strings['Type'] = 'Typ';
		$strings['Required'] = 'Povinné';
		$strings['ValidationExpression'] = 'Ověření vstupu';
		$strings['PossibleValues'] = 'Možnosti';
		$strings['SingleLineTextbox'] = 'řádkový text';
		$strings['MultiLineTextbox'] = 'víceřádkový text';
		$strings['Checkbox'] = 'zaškrtávací seznam';
		$strings['SelectList'] = 'seznam';
		$strings['CommaSeparated'] = 'oddělujte čárkou';
		$strings['Category'] = 'Kategorie';
		$strings['CategoryReservation'] = 'Rezervace';
		$strings['CategoryGroup'] = 'Skupina';
		$strings['SortOrder'] = 'Pořadí';
		$strings['Title'] = 'Nadpis';
		$strings['AdditionalAttributes'] = 'Další atributy';
		$strings['True'] = 'ano';
		$strings['False'] = 'ne';
		$strings['ForgotPasswordEmailSent'] = 'Na zadaný e-mail byly odeslány instrukce pro obnovení hesla.';
		$strings['ActivationEmailSent'] = 'Brzy obdržíte aktivační e-mail.';
		$strings['AccountActivationError'] = 'Omlouváme se, Váš učet ještě není schválen.';
		$strings['Attachments'] = 'Přílohy';
		$strings['AttachFile'] = 'Příloha';
		$strings['Maximum'] = 'limit';
		$strings['NoScheduleAdministratorLabel'] = 'Žádný rozvrh administrátora';
		$strings['ScheduleAdministrator'] = 'Rozvrh administrátora';
		$strings['Total'] = 'Celkem';
		$strings['QuantityReserved'] = 'Rezervované množství';
		$strings['AllAccessories'] = 'Veškeré vybavení';
		$strings['GetReport'] = 'Zobrazit hlášení';
		$strings['NoResultsFound'] = 'Nenalezena žádná shoda';
		$strings['SaveThisReport'] = 'Uložit hlášení';
		$strings['ReportSaved'] = 'Hlášení uloženo!';
		$strings['EmailReport'] = 'Zaslat hlášení e-mailem';
		$strings['ReportSent'] = 'Hlášení zasláno na e-mail!';
		$strings['RunReport'] = 'Spustit hlášení';
		$strings['NoSavedReports'] = 'Nemáte uložené žádné hlášení.';
		$strings['CurrentWeek'] = 'Tento týden';
		$strings['CurrentMonth'] = 'Tento měsíc';
		$strings['AllTime'] = 'Vždy';
		$strings['FilterBy'] = 'Filtrovat podle';
		$strings['Select'] = 'Výběr';
		$strings['List'] = 'Seznam';
		$strings['TotalTime'] = 'Celkový čas';
		$strings['Count'] = 'Počet';
		$strings['Usage'] = 'Užití';
		$strings['AggregateBy'] = 'Agregoval';
		$strings['Range'] = 'Rozsah';
		$strings['Choose'] = 'Nastavit';
		$strings['All'] = 'Vše';
		$strings['ViewAsChart'] = 'Zobrazit jako tabulku';
		$strings['ReservedResources'] = 'Rezervované zdroje';
		$strings['ReservedAccessories'] = 'Rezervované vybavení';
		$strings['ResourceUsageTimeBooked'] = 'Použití stroje - výběr času';
		$strings['ResourceUsageReservationCount'] = 'Použití stroje - počet rezervací';
		$strings['Top20UsersTimeBooked'] = 'Nejlepších 20 uživatelů - výběr času';
		$strings['Top20UsersReservationCount'] = 'Nejlepších 20 uživatelů - počet rezervací';
		$strings['ConfigurationUpdated'] = 'Konfigurační soubor byl upraven';
		$strings['ConfigurationUiNotEnabled'] = 'Stránka není přístupná, protože $conf[\'settings\'][\'pages\'][\'enable.configuration\'] je chybový nebo chybí.';
		$strings['ConfigurationFileNotWritable'] = 'Konfigurační soubor musí být zapisovatelný.';
		$strings['ConfigurationUpdateHelp'] = 'Více v nápovědě, části <a target=_blank href=%s>Možnosti nastavení</a>.';
		$strings['GeneralConfigSettings'] = 'Nastavení';
		$strings['UseSameLayoutForAllDays'] = 'Použít stejný rozvrh pro všechny dny';
		$strings['LayoutVariesByDay'] = 'Rozvrh pro každý den zvlášť';
		$strings['ManageReminders'] = 'E-mailová upomínka';
		$strings['ReminderUser'] = 'Číslo uživatele';
		$strings['ReminderMessage'] = 'Zpráva';
		$strings['ReminderAddress'] = 'Adresa';
		$strings['ReminderSendtime'] = 'Čas odeslání';
		$strings['ReminderRefNumber'] = 'Referenční číslo rezervace';
		$strings['ReminderSendtimeDate'] = 'Datum upomínky';
		$strings['ReminderSendtimeTime'] = 'Čas (HH:MM)';
		$strings['ReminderSendtimeAMPM'] = 'dopoledne / odpoledne';
		$strings['AddReminder'] = 'Přidat upomínku';
		$strings['DeleteReminderWarning'] = 'Opravdu to chcete?';
		$strings['NoReminders'] = 'Nemáte naplánovanou žádnou upomínku.';
		$strings['Reminders'] = 'Upomínky';
		$strings['SendReminder'] = 'Odeslat upomínku';
		$strings['minutes'] = 'minut';
		$strings['hours'] = 'hodin';
		$strings['days'] = 'den';
		$strings['ReminderBeforeStart'] = 'Před začátkem';
		$strings['ReminderBeforeEnd'] = 'Před koncem';
		$strings['Logo'] = 'Logotyp';
		$strings['CssFile'] = 'CSS Soubor';
		$strings['ThemeUploadSuccess'] = 'Změny byly uloženy. Obnovte stránku.';
		$strings['MakeDefaultSchedule'] = 'Použít jako výchozí rozvrh';
		$strings['DefaultScheduleSet'] = 'Toto je Váš výchozí rozvrh';
		$strings['FlipSchedule'] = 'Nastavit vzhled rozvrhu';
		$strings['Next'] = 'Další';
		$strings['Success'] = 'Provedeno';
		$strings['Participant'] = 'Účastník';
		$strings['ResourceFilter'] = 'Filtr zdrojů';
		$strings['ResourceGroups'] = 'Skupiny zdrojů';
		$strings['AddNewGroup'] = 'Přidat novou skupinu';
		$strings['Quit'] = 'Ukončit';
		$strings['AddGroup'] = 'Přidat skupinu';
		$strings['StandardScheduleDisplay'] = 'Použít klasické zobrazení';
		$strings['TallScheduleDisplay'] = 'Použít zobrazení na výšku';
		$strings['WideScheduleDisplay'] = 'Použít zobrazení na šířku';
		$strings['CondensedWeekScheduleDisplay'] = 'Použít zkrácené týdenní zobrazení';
		$strings['ResourceGroupHelp1'] = 'Použijte přetažení pro přemístění skupiny zdrojů.';
		$strings['ResourceGroupHelp2'] = 'Pro další akce Klikněte pravým tlačítkem na název zdroje skupiny.';
		$strings['ResourceGroupHelp3'] = 'Pro přidání zdroje do skupiny použijte přetažení.';
		$strings['ResourceGroupWarning'] = 'Pokud používáte skupiny zdroje, každý zdroj musí být přiřazen nejméně jedné skupině. Nepřiřazené zdroje nebude možné rezervovat.';
		$strings['ResourceType'] = 'Typ zdroje';
		$strings['AppliesTo'] = 'Použít na';
		$strings['UniquePerInstance'] = 'Unikátní ke každému';
		$strings['AddResourceType'] = 'Přidat typ zdroje';
		$strings['NoResourceTypeLabel'] = '(nezvolen typ zdroje)';
		$strings['ClearFilter'] = 'Vyčistit filtr';
		$strings['MinimumCapacity'] = 'Minimální kapacita';
		$strings['Color'] = 'Barva';
		$strings['Available'] = 'dostupné';
		$strings['Unavailable'] = 'obsazené';
		$strings['Hidden'] = 'skryté';
		$strings['ResourceStatus'] = 'Stav zdroje';
		$strings['CurrentStatus'] = 'Současný stav';
		$strings['AllReservationResources'] = 'Všechny zdroje';
		$strings['File'] = 'Soubor';
		$strings['BulkResourceUpdate'] = 'Hromadná aktualizace zdrojů';
		$strings['Unchanged'] = 'Nezměněné';
		$strings['Common'] = 'příbuzné';
		$strings['AdminOnly'] = 'Pouze správci';
		$strings['AdvancedFilter'] = 'Rozšířený filtr';
		$strings['MinimumQuantity'] = 'Minimální množství';
		$strings['MaximumQuantity'] = 'Maximální množství';
		$strings['ChangeLanguage'] = 'Nastavit jazyk';
		$strings['AddRule'] = 'Přidat roli';
		$strings['Attribute'] = 'Attribut';
		$strings['RequiredValue'] = 'Vyžadovaná hodnota';
		$strings['ReservationCustomRuleAdd'] = 'Použít barvu, má-li atribut hodnotu';
		$strings['AddReservationColorRule'] = 'Přidat roli Barevné zvýraznění';
		$strings['LimitAttributeScope'] = 'Sdružit ve specifických případech';
		$strings['CollectFor'] = 'Sdružit pro';
		$strings['SignIn'] = 'Přihlášení';
		$strings['AllParticipants'] = 'Všichni účastníci';
		$strings['RegisterANewAccount'] = 'Registrace';
		$strings['Dates'] = 'Data';
		$strings['More'] = 'Více';
		$strings['ResourceAvailability'] = 'Dostupnost zdrojů';
		$strings['UnavailableAllDay'] = 'Nedostupné celý den';
		$strings['AvailableUntil'] = 'Dostupných časů k rezervaci';
		$strings['AvailableBeginningAt'] = 'Dostupný začátek';
        $strings['AvailableAt'] = 'K dizpozici od';
		$strings['AllResourceTypes'] = 'Všechny typy zdrojů';
		$strings['AllResourceStatuses'] = 'Všechny stavy';
		$strings['AllowParticipantsToJoin'] = 'pozvaní se mohou připojit';
		$strings['Join'] = 'Připojit se';
		$strings['YouAreAParticipant'] = 'Jste účastníkem rezervace';
		$strings['YouAreInvited'] = 'Jste přizván k rezervaci';
		$strings['YouCanJoinThisReservation'] = 'K revervaci se můžete připojit';
		$strings['Import'] = 'Import';
		$strings['GetTemplate'] = 'Získat šablonu';
		$strings['UserImportInstructions'] = '<ul><li>Soubor musí být v CSV formatu.</li><li>Je vyžadováno uživatelské jméno a e-mail.</li><li>Nebude vynucována platnost atributů.</li><li>Prázdné položky budou nahrazeny výchozími hodnotami, \'heslo\' pak heslem uživatele.</li><li>Šablonu použijte jako vzor.</li></ul>';
		$strings['RowsImported'] = 'Importované řádky';
		$strings['RowsSkipped'] = 'Vynechané řádky';
		$strings['Columns'] = 'Sloupce';
		$strings['Reserve'] = 'Rezervovat';
		$strings['AllDay'] = 'celodenní';
		$strings['Everyday'] = 'každodenní';
		$strings['IncludingCompletedReservations'] = 'Včetně dokončených rezervací';
		$strings['NotCountingCompletedReservations'] = 'Bez dokončených rezervací';
		$strings['RetrySkipConflicts'] = 'Přeskočit konfliktní rezervace';
		$strings['Retry'] = 'Opakovat';
		$strings['RemoveExistingPermissions'] = 'Odebrat nynější oprávnění?';
		$strings['Continue'] = 'Pokračovat';
		$strings['WeNeedYourEmailAddress'] = 'Pro rezervaci je e-mail nezbytný';
		$strings['ResourceColor'] = 'Barva zdroje';
		$strings['DateTime'] = 'Datum a čas';
		$strings['AutoReleaseNotification'] = 'Nebude-li zarezervováno, po %s minutách dojde k uvolnění';
		$strings['RequiresCheckInNotification'] = 'Vyžaduje rezervaci/zrušení';
		$strings['NoCheckInRequiredNotification'] = 'Nevyžaduje rezervaci/zrušení';
		$strings['RequiresApproval'] = 'Vyžaduje schválení';
		$strings['CheckingIn'] = 'Rezervování';
		$strings['CheckingOut'] = 'Uvolňování';
		$strings['CheckIn'] = 'Rezervovat';
		$strings['CheckOut'] = 'Uvolnit';
		$strings['ReleasedIn'] = 'Uvolněno';
		$strings['CheckedInSuccess'] = 'Rezervováno';
		$strings['CheckedOutSuccess'] = 'Uvolněno';
		$strings['CheckInFailed'] = 'Neúspěšná rezervace';
		$strings['CheckOutFailed'] = 'Neúspěšné uvolnění';
		$strings['CheckInTime'] = 'Čas rezervace';
		$strings['CheckOutTime'] = 'Čas uvolnění';
		$strings['OriginalEndDate'] = 'Původní konec';
		$strings['SpecificDates'] = 'zvolené dny';
		$strings['Users'] = 'Uživatelé';
		$strings['Guest'] = 'Host';
		$strings['ResourceDisplayPrompt'] = 'Zozbrazení zdroje';
		$strings['Credits'] = 'Kredity';
		$strings['AvailableCredits'] = 'Dostupné kredity';
		$strings['CreditUsagePerSlot'] = 'Vyžaduje %s kreditů na časový úsek (mimo špičku)';
		$strings['PeakCreditUsagePerSlot'] = 'Vyžaduje %s kreditů na časový úsek (ve špičce)';
		$strings['CreditsRule'] = 'Nemáte dostatek kreditů. Vyžadováno: %s. Na účtu: %s';
		$strings['PeakTimes'] = 'Časy špiček';
		$strings['AllYear'] = 'Celý rok';
		$strings['MoreOptions'] = 'Další podmíny';
		$strings['SendAsEmail'] = 'Odeslat jako e-mail';
		$strings['UsersInGroups'] = 'Uživatelé ve skupině';
		$strings['UsersWithAccessToResources'] = 'Uživatelé s přístupem ke zdrojům';
		$strings['AnnouncementSubject'] = 'Nové oznámení od %s';
		$strings['AnnouncementEmailNotice'] = 'bude zasláno e-mailem';
		$strings['Day'] = 'Den';
		$strings['NotifyWhenAvailable'] = 'Upozornit mě, bude-li k dispozici';
		$strings['AddingToWaitlist'] = 'Přidá vás k čekajícím';
		$strings['WaitlistRequestAdded'] = 'Budete upozorněni, uvolní-li se termín';
		$strings['PrintQRCode'] = 'Vytisknout QR kód';
		$strings['FindATime'] = 'Najít termín';
		$strings['AnyResource'] = 'Všechny zdroje';
		$strings['ThisWeek'] = 'Tento týden';
		$strings['Hours'] = 'Hodiny';
		$strings['Minutes'] = 'Minuty';
        $strings['ImportICS'] = 'Importovat z ICS';
        $strings['ImportQuartzy'] = 'Importovat z Quartzy';
        $strings['OnlyIcs'] = 'Nahrát lze pouze *.ics soubory.';
        $strings['IcsLocationsAsResources'] = 'Lokality budou nahrány jako zdroje.';
        $strings['IcsMissingOrganizer'] = 'Všem událostem bez organizátora bude aktuální uživatel nastaven jako vlastník.';
        $strings['IcsWarning'] = 'Nebudou uplatněna rezervační pravidla - jsou možné konflikty, duplicity atd.';
		$strings['BlackoutAroundConflicts'] = 'Odstávky obklopující konfliktní rezervace';
		$strings['DuplicateReservation'] = 'Duplikovat';
		$strings['UnavailableNow'] = 'Není k dispozici';
		$strings['ReserveLater'] = 'Rezervovat později';
		$strings['CollectedFor'] = 'Sdružit pro';
		$strings['IncludeDeleted'] = 'Včetně odstraněných rezervací';
		$strings['Deleted'] = 'Odstraněno';
		$strings['Back'] = 'Zpět';
		$strings['Forward'] = 'Vpřed';
		$strings['DateRange'] = 'Časový úsek';
		$strings['Copy'] = 'Kopírovat';
		$strings['Detect'] = 'Zjistit';
		$strings['Autofill'] = 'Automatické vyplnění';
		$strings['NameOrEmail'] = 'jméno nebo e-mail';
		$strings['ImportResources'] = 'Importovat zdroje';
		$strings['ExportResources'] = 'Exportovat zdroje';
		$strings['ResourceImportInstructions'] = '<ul><li>Soubor musí být v CSV formatu a UTF-8 kódování.</li><li>Je vyžadováno uživatelské jméno. Prázdné položky budou nahrazeny výchozími hodnotami.</li><li>Možné stavy jsou \'Dostupný\', \'Nedostupný\' and \'Skrytý\'.</li><li>Barva jako hex hodnota, např.) #ffffff.</li><li>Sloupce automatického přiřazení a schválení mohou být pravda nebo nepravda.</li><li>Nebude vynucována platnost atributů.</li><li>Čárka odděluje skupiny zdrojů.</li><li>Šablonu použijte jako vzor.</li></ul>';
		$strings['ReservationImportInstructions'] = '<ul><li>Soubor musí být v CSV formatu a UTF-8 kódování.</li><li>Je vyžadován e-mail, názvy zdroje, začátek a konec.</li><li>Začátek a konec jako úplný datum a čas. Doporučený formát je YYYY-mm-dd HH:mm (2017-12-31 20:30).</li><li>Pravidla, konflikty a platnost časových úseků nebudou kontrolovány.</li><li>Oznámení nebudou odeslána.</li><li>Nebude vynucována platnost atributů.</li><li>Čárka odděluje skupiny zdrojů.</li><li>Šablonu použijte jako vzor.</li></ul>';
		$strings['AutoReleaseMinutes'] = 'Automatické uvolnění - minuty';
		$strings['CreditsPeak'] = 'Kredity (ve špičce)';
		$strings['CreditsOffPeak'] = 'Kredity (mimo špičku)';
		$strings['ResourceMinLengthCsv'] = 'Minimální délka rezervace';
		$strings['ResourceMaxLengthCsv'] = 'Maximální délka rezervace';
		$strings['ResourceBufferTimeCsv'] = 'Časový odstup';
		$strings['ResourceMinNoticeAddCsv'] = 'Přidat minimální oznámení rezervace';
		$strings['ResourceMinNoticeUpdateCsv'] = 'Aktualizovat minimální oznámení rezervace';
		$strings['ResourceMinNoticeDeleteCsv'] = 'Smazat minimální oznámení rezervace';
		$strings['ResourceMaxNoticeCsv'] = 'Nejzazší konec rezervace';
		$strings['Export'] = 'Export';
		$strings['DeleteMultipleUserWarning'] = 'Odstraněním uživatelů odstraníte všechny jejich současné, budoucí i dřívější rezervace. Žádné e-maily nebudou rozeslány.';
		$strings['DeleteMultipleReservationsWarning'] = 'Žádné e-maily nebudou rozeslány.';
		$strings['ErrorMovingReservation'] = 'Chyba při přesunu rezervace';
        $strings['SelectUser'] = 'Vybrat uživatele';
        $strings['InviteUsers'] = 'Pozvat uživatele';
        $strings['InviteUsersLabel'] = 'Zadejte e-maily zvaných';
        $strings['ApplyToCurrentUsers'] = 'Nastavit stávajícím uživatelům';
        $strings['ReasonText'] = 'Odůvodnění';
        $strings['NoAvailableMatchingTimes'] = 'Žádné dostupné časy neodpovídají vašemu zadání';
        $strings['Schedules'] = 'Rozvrhy';
        $strings['NotifyUser'] = 'Oznámit uživateli';
        $strings['UpdateUsersOnImport'] = 'Aktualizovat uživatele s e-mailem';
        $strings['UpdateResourcesOnImport'] = 'Aktulizovat pojmenované zdroje';
        $strings['Reject'] = 'Odmítnout';
        $strings['CheckingAvailability'] = 'Kontroluji dostupnost';
        $strings['CreditPurchaseNotEnabled'] = 'Nemáte oprávnění zakoupit kredity';
        $strings['CreditsCost'] = 'Cena jednotlivých kreditů';
        $strings['Currency'] = 'Měna';
        $strings['PayPalClientId'] = 'ID uživatele';
        $strings['PayPalSecret'] = 'Tajné';
        $strings['PayPalEnvironment'] = 'Prostředí';
        $strings['Sandbox'] = 'Izolovaný prostor';
        $strings['Live'] = 'Živě';
        $strings['StripePublishableKey'] = 'Veřejný klíč';
        $strings['StripeSecretKey'] = 'Tajný klíč';
        $strings['CreditsUpdated'] = 'Byla aktualizována cena kreditů';
        $strings['GatewaysUpdated'] = 'Byly aktualizovány platební brány';
        $strings['PurchaseSummary'] = 'Celkový nákup';
        $strings['EachCreditCosts'] = 'Cena jednotlivých kreditů';
        $strings['Checkout'] = 'Pokladna';
        $strings['Quantity'] = 'Množství';
        $strings['CreditPurchase'] = 'Uhradit z kreditu';
        $strings['EmptyCart'] = 'Prázdný košík.';
        $strings['BuyCredits'] = 'Zakoupit kredity';
        $strings['CreditsPurchased'] = 'kredity zakoupeny.';
        $strings['ViewYourCredits'] = 'Zobrazit mé kredity';
        $strings['TryAgain'] = 'Zkusit znovu';
        $strings['PurchaseFailed'] = 'Platba se nezdařila.';
        $strings['NoteCreditsPurchased'] = 'Kredity zakoupeny';
        $strings['CreditsUpdatedLog'] = 'Aktualizace kreditů - %s';
        $strings['ReservationCreatedLog'] = 'Vytvoření rezervace. Referenční číslo %s';
        $strings['ReservationUpdatedLog'] = 'Aktualizace rezervace. Referenční číslo %s';
        $strings['ReservationDeletedLog'] = 'Odstranění rezervace. Referenční číslo %s';
        $strings['BuyMoreCredits'] = 'Zakoupit více kreditů';
        $strings['Transactions'] = 'Transakce';
        $strings['Cost'] = 'Cena';
        $strings['PaymentGateways'] = 'Platební brány';
        $strings['CreditHistory'] = 'Historie kreditů';
        $strings['TransactionHistory'] = 'Historie transakcí';
        $strings['Date'] = 'Datum';
        $strings['Note'] = 'Poznámka';
        $strings['CreditsBefore'] = 'Kreditů před';
        $strings['CreditsAfter'] = 'Kreditů po';
        $strings['TransactionFee'] = 'Transakční poplatek';
        $strings['InvoiceNumber'] = 'Číslo faktury';
        $strings['TransactionId'] = 'ID transakce';
        $strings['Gateway'] = 'Brána';
        $strings['GatewayTransactionDate'] = 'Datum transakce brány';
        $strings['Refund'] = 'Vrátit peníze';
        $strings['IssueRefund'] = 'Vrácení daně';
        $strings['RefundIssued'] = 'Úspěšné vrácení daně';
        $strings['RefundAmount'] = 'Vrácená částka';
        $strings['AmountRefunded'] = 'Vráceno';
        $strings['FullyRefunded'] = 'Plně vráceno';
        $strings['YourCredits'] = 'Vaše kredity';
        $strings['PayWithCard'] = 'Uhradit kartou';
        $strings['or'] = 'nebo';
        $strings['CreditsRequired'] = 'Vyžaduje kredity';
        $strings['AddToGoogleCalendar'] = 'Přidat do Google kalendáře';
        $strings['Image'] = 'Obrázek';
        $strings['ChooseOrDropFile'] = 'Zvolte soubor nebo jej sem přetáhněte';
        $strings['SlackBookResource'] = 'Rezervovat %s';
        $strings['SlackBookNow'] = 'Rezervovat';
        $strings['SlackNotFound'] = 'Takto pojmenovaný zdroj nebyl nalezen. Rezervovat - zahájí novou rezervaci.';
        $strings['AutomaticallyAddToGroup'] = 'Nové uživatele automaticky přidávat do této skupiny';
        $strings['GroupAutomaticallyAdd'] = 'Automaticky přidávat';
        $strings['TermsOfService'] = 'Podmínky užití';
        $strings['EnterTermsManually'] = 'zadat ručně';
        $strings['LinkToTerms'] = 'odkazem';
        $strings['UploadTerms'] = 'nahrát';
        $strings['RequireTermsOfServiceAcknowledgement'] = 'Podmínky užití užití jsou vyžadovány';
        $strings['UponReservation'] = 'při rezervaci';
        $strings['UponRegistration'] = 'při registraci';
        $strings['ViewTerms'] = 'Zobrazit podmínky užití';
        $strings['IAccept'] = 'Přijímám';
        $strings['TheTermsOfService'] = 'Podmínky užití';
        $strings['DisplayPage'] = 'Zobrazit stránku';
        $strings['AvailableAllYear'] = 'Celý rok';
        $strings['Availability'] = 'Dostupnost';
        $strings['AvailableBetween'] = 'Dostupnost mezi';
        $strings['ConcurrentYes'] = 'Zdroje mohou být rezervovány více osobami na stejný čas';
        $strings['ConcurrentNo'] = 'Zdroje nemohou být rezervovány více osobami na stejný čas';
        $strings['ScheduleAvailabilityEarly'] = ' Rozvrh dosud nebyl dostupný. Je k dispozici';
        $strings['ScheduleAvailabilityLate'] = 'Rozvrh již není dostupný. Není k dispozici';
        $strings['ResourceImages'] = 'Zdroj obrázků';
        $strings['FullAccess'] = 'úplný přístup';
        $strings['ViewOnly'] = 'pouze zobrazení';
        $strings['Purge'] = 'Smazat';
        $strings['UsersWillBeDeleted'] = 'uživatelé budou odstraněni';
        $strings['BlackoutsWillBeDeleted'] = 'odstávky budou odstraněny';
        $strings['ReservationsWillBePurged'] = 'rezervace budou odstraněny';
        $strings['ReservationsWillBeDeleted'] = 'rezervace budou odstraněny';
        $strings['PermanentlyDeleteUsers'] = 'Trvale smazat uživatele, kteří se nepříhlásili od';
        $strings['DeleteBlackoutsBefore'] = 'Smazat odstávky před';
        $strings['DeletedReservations'] = 'Smazané rezervace';
        $strings['DeleteReservationsBefore'] = 'Smazat rezervace před';
        $strings['SwitchToACustomLayout'] = 'Přepnout na uživatelské rozložení';
        $strings['SwitchToAStandardLayout'] = 'Přepnout na standardní rozložení';
        $strings['ThisScheduleUsesACustomLayout'] = 'Tento rozvrh užívá uživatelské rozložení';
        $strings['ThisScheduleUsesAStandardLayout'] = 'Tento rozvrh užívá standardní rozložení';
        $strings['SwitchLayoutWarning'] = 'Opravdu chcete změnit typ rozložení? Odtraníte tím všechny existující časové úseky.';
        $strings['DeleteThisTimeSlot'] = 'Smazat tento časový úsek?';
        $strings['Refresh'] = 'Aktualizovat';
        $strings['ViewReservation'] = 'Zobrazit rezervace';
        $strings['PublicId'] = 'Veřejné Id';
        $strings['Public'] = 'Veřejné';
        $strings['AtomFeedTitle'] = '%s rezervace';
        $strings['DefaultStyle'] = 'Výchozí styl';
        $strings['Standard'] = 'Standard';
        $strings['Wide'] = 'Šířka';
        $strings['Tall'] = 'Výška';
        $strings['EmailTemplate'] = 'Šablona e-mailu';
        $strings['SelectEmailTemplate'] = 'zvolte šablonu';
        $strings['ReloadOriginalContents'] = 'Znovunačíst původní obsah';
        $strings['UpdateEmailTemplateSuccess'] = 'Šablona e-mailu byla aktualizována';
        $strings['UpdateEmailTemplateFailure'] = 'Nezdařila se aktualizace šablony e-mailu. Ujistěte se, že máte právo zapisovat do adresáře.';
        $strings['BulkResourceDelete'] = 'Hromadné odstranění zdrojů';
        $strings['NewVersion'] = 'Nová verze!';
        $strings['WhatsNew'] = 'Co je nového?';
        $strings['OnlyViewedCalendar'] = 'Tento plán může být zobrazen pouze z rozvrhu';
        $strings['Grid'] = 'Mřížka';
        $strings['List'] = 'Seznam';
        $strings['NoReservationsFound'] = 'Nenalezeny žádné rezervace';
        $strings['EmailReservation'] = 'Rezervační e-mail';
        $strings['AdHocMeeting'] = 'Ad hoc schůzka';
        $strings['NextReservation'] = 'Další rezervace';
        $strings['MissedCheckin'] = 'Zmeškaná přihlášení';
        $strings['MissedCheckout'] = 'Zmeškaná odhlášení';
        // End Strings

		// Install
		$strings['InstallApplication'] = 'Instalovat Booked';
		$strings['IncorrectInstallPassword'] = 'Lituji, nesprávné heslo.';
		$strings['SetInstallPassword'] = 'Před spuštěním instalace musíte nastavit instlační heslo.';
		$strings['InstallPasswordInstructions'] = 'V %s nastavte %s jako náhodné a obtížně uhodnutelné heslo, potom se vraťte na tuto stránku.<br/>Můžete použít %s';
		$strings['NoUpgradeNeeded'] = 'Nejnovější verze Booked. Není třeba aktualizovat.';
		$strings['ProvideInstallPassword'] = 'Prosím, zadejte vaše instalační heslo.';
		$strings['InstallPasswordLocation'] = 'Pro %s nebylo nalezeno v %s.';
		$strings['VerifyInstallSettings'] = 'Před pokračováním ověřte následující výchozí nastavení, nebo jej změňte v %s.';
		$strings['DatabaseName'] = 'Název databáze';
		$strings['DatabaseUser'] = 'Uživatel databáze';
		$strings['DatabaseHost'] = 'Hostitel databáze';
		$strings['DatabaseCredentials'] = 'Je třeba zvolit uživatele MySQL s právy vytvářet databáze. Neznáte-li jej, kontaktujte správce databáze. Ve většině případů bude fungovat root.';
		$strings['MySQLUser'] = 'Uživatel MySQL';
		$strings['InstallOptionsWarning'] = 'Následující možnosti pravděpodobně nebudou fungovat v hostovaném prostředí. Instalujete-li do takového prostředí, použijte průvodce MySQL pro dokončení těchto kroků.';
		$strings['CreateDatabase'] = 'Vytvořit databázi';
		$strings['CreateDatabaseUser'] = 'Vytvořit uživatele databáze';
		$strings['PopulateExampleData'] = 'Import ukázkových dat. Vytvoří účet administrátora: admin/password a uživatele: user/password';
		$strings['DataWipeWarning'] = 'Varování: Budou odstraněna veškerá data';
		$strings['RunInstallation'] = 'Spustit instalaci';
		$strings['UpgradeNotice'] = 'Povyšujete z verze <b>%s</b> na <b>%s</b>';
		$strings['RunUpgrade'] = 'Zahájit povýšení';
		$strings['Executing'] = 'Provádění';
		$strings['StatementFailed'] = 'Selhalo. Podrobnosti:';
		$strings['SQLStatement'] = 'SQL příkaz:';
		$strings['ErrorCode'] = 'Kód chyby:';
		$strings['ErrorText'] = 'Text chyby:';
		$strings['InstallationSuccess'] = 'Instlace byla úspěšně dokončena!';
		$strings['RegisterAdminUser'] = 'Zaregistrujte administrátorský účet. To je zapotřebí, pokud jste neimporovali ukázková data. Ujistěte se, že $conf[\'settings\'][\'allow.self.registration\'] = \'true\' ve vašem %s souboru.';
		$strings['LoginWithSampleAccounts'] = 'Pokud jste importovali ukázková data, můžete se přihlásit jako admin/password k administrátorskému účtu nebo user/password k účtu běžnému.';
		$strings['InstalledVersion'] = 'Booked provozujete ve verzi %s';
		$strings['InstallUpgradeConfig'] = 'Doporučuje se provést povýšení konfiguračního souboru';
		$strings['InstallationFailure'] = 'Při instalaci došlo k problémům. Opravte je a opakujte instalaci.';
		$strings['ConfigureApplication'] = 'Konfigurovat Booked';
		$strings['ConfigUpdateSuccess'] = 'Váš konfigurační soubor je nyní aktuální!';
		$strings['ConfigUpdateFailure'] = 'Nemohli jsme automaticky aktualizovat váš konfigurační soubor. Přepíšte prosím obsah config.php následujícím způsobem:';
		$strings['ScriptUrlWarning'] = 'Vaše nastavení <em>script.url</em> patrně není správné. Hodnota <strong>%s</strong> by měla nejspíš být <strong>%s</strong>';
		// End Install

		// Errors
		$strings['LoginError'] = 'Chybně zadané uživatelské jméno nebo heslo.';
		$strings['ReservationFailed'] = 'Vaše rezervace nemůže být vytvořena.';
		$strings['MinNoticeError'] = 'Tato rezervace musí obsahovat popis. Nejstarší datum, které může být rezervováno je %s.';
		$strings['MinNoticeErrorUpdate'] = 'Změna této rezervace vyžaduje předchozí oznámení. Rezervace před %s nelze měnit.';
		$strings['MinNoticeErrorDelete'] = 'Odstranění této rezervace vyžaduje předchozí oznámení. Rezervace před %s nelze odstranit.';
		$strings['MaxNoticeError'] = 'Tato rezervace nemůže být naplánována tak daleko. Nejzazší datum, které může být rezervováno je %s.';
		$strings['MinDurationError'] = 'Tato rezervace musí být delší než %s.';
		$strings['MaxDurationError'] = 'Tato rezervace nemůže trvat déle než %s.';
		$strings['ConflictingAccessoryDates'] = 'Překročili jste omezený počet kusů';
		$strings['NoResourcePermission'] = 'Nemáte oprávnění pro vstup k jednomu nebo více požadovaným zdrojům';
		$strings['ConflictingReservationDates'] = 'Zde je výpis rezervací, které jsou v konfliktu s Vámi vytvořenou:';
		$strings['StartDateBeforeEndDateRule'] = 'Začátek rezervace musí začínat dříve než její konec.';
		$strings['StartIsInPast'] = 'Začátek rezervace nemůže být vytvořen v minulosti';
		$strings['EmailDisabled'] = 'Administrátor zakázal posílání e-mailových oznámení.';
		$strings['ValidLayoutRequired'] = 'Časový úsek musí být vytvořen na celý den - 24hodin';
		$strings['CustomAttributeErrors'] = 'Chybné s dalšími hodnotami:';
		$strings['CustomAttributeRequired'] = '%s je povinné pole';
		$strings['CustomAttributeInvalid'] = 'Hodnota pro %s je chybná';
		$strings['AttachmentLoadingError'] = 'Omlouváme se, došlo k chybě při načítání požadovaného souboru.';
		$strings['InvalidAttachmentExtension'] = 'Můžete nahrát pouze soubory těchto typů: %s';
		$strings['InvalidStartSlot'] = 'Začátek a čas rezervace není je chybně zadán.';
		$strings['InvalidEndSlot'] = 'Konec a čas rezervace je chybně zadán.';
		$strings['MaxParticipantsError'] = '%s můžete mít pouze %s participantů.';
		$strings['ReservationCriticalError'] = 'Závažná chyba při rezervaci. Kontaktuje nás prosím.';
		$strings['InvalidStartReminderTime'] = 'Začátek upomínky je chybně zadán.';
		$strings['InvalidEndReminderTime'] = 'Konec upomínky je chybně zadán.';
		$strings['QuotaExceeded'] = 'Překročena kvóta.';
		$strings['MultiDayRule'] = '%s není povolené rezervovat na několik dní.';
		$strings['InvalidReservationData'] = 'Nastal problém při požadavku na rezervaci.';
		$strings['PasswordError'] = 'Heslo musí obsahovat nejméně %s písměn a nejméně %s čísel.';
		$strings['PasswordErrorRequirements'] = 'Heslo musí obsahovat kombinaci nejméně %s malých a velkých písmen a %s čísel.';
		$strings['NoReservationAccess'] = 'Nemáte povolení měnit tuto rezervaci.';
		$strings['PasswordControlledExternallyError'] = 'Vaše heslo je spravováno externím systémem, tudíž jej zde nelze měnit.';
		$strings['AccessoryResourceRequiredErrorMessage'] = 'Vybavení %s lze rezervovat jen společně s %s';
		$strings['AccessoryMinQuantityErrorMessage'] = 'Musíte rezervovat nejméně %s vybavení %s';
		$strings['AccessoryMaxQuantityErrorMessage'] = 'Nemůžete rezervovat více než %s vybavení %s';
		$strings['AccessoryResourceAssociationErrorMessage'] = 'Vybavení \'%s\' nemůže být rezervováno s požadovanými zdroji';
		$strings['NoResources'] = 'Nebyl přidán zdroj.';
		$strings['ParticipationNotAllowed'] = 'Nemáte oprávnění se připojit k této rezervaci.';
		$strings['ReservationCannotBeCheckedInTo'] = 'Tuto rezervaci nelze potvrdit.';
		$strings['ReservationCannotBeCheckedOutFrom'] = 'Tuto rezervaci nelze odhlásit.';
		$strings['InvalidEmailDomain'] = 'E-mailová adresa nevyhovuje žádné z povolených domén';
		$strings['TermsOfServiceError'] = 'Podmínky užití musíte přijmout';
		$strings['UserNotFound'] = 'Uživatel nebyl nalezen';
		$strings['ScheduleAvailabilityError'] = 'Tento rozvrh je dostupný mezi %s a %s';
		$strings['ReservationNotFoundError'] = 'Rezervace nelanlezena';
		$strings['ReservationNotAvailable'] = 'Rezervace není k dispozice';
		$strings['TitleRequiredRule'] = 'Je vyžadován název rezervace';
		$strings['DescriptionRequiredRule'] = 'Je vyžadován popis rezervace';
		$strings['WhatCanThisGroupManage'] = 'Co může tato skupina spravovat?';
		// End Errors

		// Page Titles
		$strings['CreateReservation'] = 'Rezervování';
		$strings['EditReservation'] = 'Upravování rezervace';
		$strings['LogIn'] = 'Přihlášení';
		$strings['ManageReservations'] = 'Rezervace';
		$strings['AwaitingActivation'] = 'Čeká na aktivaci';
		$strings['PendingApproval'] = 'Probíhá schválení';
		$strings['ManageSchedules'] = 'Rozvrhy';
		$strings['ManageResources'] = 'Zdroje';
		$strings['ManageAccessories'] = 'Vybavení';
		$strings['ManageUsers'] = 'Uživatelé';
		$strings['ManageGroups'] = 'Skupiny';
		$strings['ManageQuotas'] = 'Kvóty';
		$strings['ManageBlackouts'] = 'Termíny odstávek';
		$strings['MyDashboard'] = 'Hlavní strana';
		$strings['ServerSettings'] = 'Informace o serveru';
		$strings['Dashboard'] = 'Panel';
		$strings['Help'] = 'Nápověda';
		$strings['Administration'] = 'Administrace';
		$strings['About'] = 'O aplikaci';
		$strings['Bookings'] = 'Rezervace';
		$strings['Schedule'] = 'Rezervace';
		$strings['Account'] = 'Účet';
		$strings['EditProfile'] = 'Upravit vlastní profil';
		$strings['FindAnOpening'] = 'Najít otevření';
		$strings['OpenInvitations'] = 'Zobrazit pozvánky';
		$strings['ResourceCalendar'] = 'Rozvrh zdrojů';
		$strings['Reservation'] = 'Nová rezervace';
		$strings['Install'] = 'Instalace';
		$strings['ChangePassword'] = 'Heslo';
		$strings['MyAccount'] = 'Můj účet';
		$strings['Profile'] = 'Nastavení profilu';
		$strings['ApplicationManagement'] = 'Správa systému';
		$strings['ForgotPassword'] = 'zapomenuté heslo';
		$strings['NotificationPreferences'] = 'Nastavení e-mailových oznámení'; 
		$strings['ManageAnnouncements'] = 'Odstávky';
		$strings['Responsibilities'] = 'Odpovědnost';
		$strings['GroupReservations'] = 'Skupinové rezervace';
		$strings['ResourceReservations'] = 'Zdroje rezervací';
		$strings['Customization'] = 'Rozšířitelnost';
		$strings['Attributes'] = 'Pole';
		$strings['AccountActivation'] = 'Aktivace účtů';
		$strings['ScheduleReservations'] = 'Rozvrh zdrojů';
		$strings['Reports'] = 'Hlášení';
		$strings['GenerateReport'] = 'Vytvořit nové hlášení';
		$strings['MySavedReports'] = 'Má uložená hlášení';
		$strings['CommonReports'] = 'Společná hlášení';
		$strings['ViewDay'] = 'Zobrazit denní přehled';
		$strings['Group'] = 'Skupina';
		$strings['ManageConfiguration'] = 'Globální nastavení';
		$strings['LookAndFeel'] = 'Nastavení vzhledu';
		$strings['ManageResourceGroups'] = 'Skupiny zdroje';
		$strings['ManageResourceTypes'] = 'Typy zdroje';
		$strings['ManageResourceStatus'] = 'Stavy zdroje';
		$strings['ReservationColors'] = 'Barevné zvýraznění';
		$strings['SearchReservations'] = 'Vyhledat rezervace';
		$strings['ManagePayments'] = 'Platby';
		$strings['ViewCalendar'] = 'Zobrazit rozvrh';
		$strings['DataCleanup'] = 'Vymazání dat';
		$strings['ManageEmailTemplates'] = 'Správa šablon e-mailů';
		// End Page Titles

		// Day representations
		$strings['DaySundaySingle'] = 'Ne';
		$strings['DayMondaySingle'] = 'Po';
		$strings['DayTuesdaySingle'] = 'Út';
		$strings['DayWednesdaySingle'] = 'St';
		$strings['DayThursdaySingle'] = 'Čt';
		$strings['DayFridaySingle'] = 'Pá';
		$strings['DaySaturdaySingle'] = 'So';

		$strings['DaySundayAbbr'] = 'Ne';
		$strings['DayMondayAbbr'] = 'Po';
		$strings['DayTuesdayAbbr'] = 'Út';
		$strings['DayWednesdayAbbr'] = 'St';
		$strings['DayThursdayAbbr'] = 'Čt';
		$strings['DayFridayAbbr'] = 'Pá';
		$strings['DaySaturdayAbbr'] = 'So';
		// End Day representations

		// Email Subjects
		$strings['ReservationApprovedSubject'] = 'Vaše rezervace byla potvrzena';
		$strings['ReservationCreatedSubject'] = 'Rezervace byla vytvořena';
		$strings['ReservationUpdatedSubject'] = 'Rezervace byla upravena';
		$strings['ReservationDeletedSubject'] = 'Rezervace byla zrušena';
		$strings['ReservationCreatedAdminSubject'] = 'Oznámení: rezervace vytvořena';
		$strings['ReservationUpdatedAdminSubject'] = 'Oznámení: rezervace upravena';
		$strings['ReservationDeleteAdminSubject'] = 'Oznámení: rezervace zrušena';
		$strings['ReservationApprovalAdminSubject'] = 'Oznámení: Rezervace vyžaduje vaše schválení';
		$strings['ParticipantAddedSubject'] = 'Oznámení: rezervace účastníků';
		$strings['ParticipantDeletedSubject'] = 'Rezervace zrušena';
		$strings['InviteeAddedSubject'] = 'Pozvánka do rezervace';
		$strings['ResetPasswordRequest'] = 'Požadavek na resetování hesla';
		$strings['ActivateYourAccount'] = 'Prosíme, aktivujte svůj účet';
		$strings['ReportSubject'] = 'Vaše požadovaná hlášení (%s)';
		$strings['ReservationStartingSoonSubject'] = 'Vaše rezervace %s zanedlouho začne';
		$strings['ReservationEndingSoonSubject'] = 'Vaše rezervace %s zanedlouho končí';
		$strings['UserAdded'] = 'Byl přidán nový uživatel';
		$strings['UserDeleted'] = 'Uživatelský účet %s smazal %s';
		$strings['GuestAccountCreatedSubject'] = 'Podrobnosti vašeho účtu %s';
		$strings['AccountCreatedSubject'] = 'Podrobnosti vašeho účtu %s';
		$strings['InviteUserSubject'] = '%s vás pozval k připojení se k %s';

		$strings['ReservationApprovedSubjectWithResource'] = 'Rezervace byla schválena pro %s';
		$strings['ReservationCreatedSubjectWithResource'] = 'Rezervace vytvořena pro %s';
		$strings['ReservationUpdatedSubjectWithResource'] = 'Rezervace aktualizována pro %s';
		$strings['ReservationDeletedSubjectWithResource'] = 'Rezervace odstraněna %s';
		$strings['ReservationCreatedAdminSubjectWithResource'] = 'Oznámení: Rezervace vytvořena pro %s';
		$strings['ReservationUpdatedAdminSubjectWithResource'] = 'Oznámení: Rezervace aktualizována pro %s';
		$strings['ReservationDeleteAdminSubjectWithResource'] = 'Oznámení: Rezervace odstraněna pro %s';
		$strings['ReservationApprovalAdminSubjectWithResource'] = 'Oznámení: Rezervace pro %s vyžaduje vaše schválení';
		$strings['ParticipantAddedSubjectWithResource'] = '%s vás přidal k rezervaci %s';
		$strings['ParticipantDeletedSubjectWithResource'] = '%s vás odebral z rezervace %s';
		$strings['InviteeAddedSubjectWithResource'] = '%s vás zve k rezervaci %s';
		$strings['MissedCheckinEmailSubject'] = 'Zmeškané registrace pro %s';
		$strings['ReservationShareSubject'] = '%s sdílí rezervaci %s';
		// End Email Subjects

		$this->Strings = $strings;

		return $this->Strings;
	}

	/**
	 * @return array
	 */
	protected function _LoadDays()
	{
		$days = array();

		/***
		 * DAY NAMES
		 * All of these arrays MUST start with Sunday as the first element
		 * and go through the seven day week, ending on Saturday
		 ***/
		// The full day name
		$days['full'] = array('Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota');
		// The three letter abbreviation
		$days['abbr'] = array('Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So');
		// The two letter abbreviation
		$days['two'] = array('Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So');
		// The one letter abbreviation
		$days['letter'] = array('N', 'P', 'Ú', 'S', 'Č', 'P', 'S');

		$this->Days = $days;

		return $this->Days;
	}

	/**
	 * @return array
	 */
	protected function _LoadMonths()
	{
		$months = array();

		/***
		 * MONTH NAMES
		 * All of these arrays MUST start with January as the first element
		 * and go through the twelve months of the year, ending on December
		 ***/
		// The full month name
		$months['full'] = array('Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec');
		// The three letter month name
		$months['abbr'] = array('Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čen', 'Čec', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro');

		$this->Months = $months;

		return $this->Months;
	}

	/**
	 * @return array
	 */
	protected function _LoadLetters()
	{
		$this->Letters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

		return $this->Letters;
	}

	protected function _GetHtmlLangCode()
	{
		return 'cz';
	}
}
